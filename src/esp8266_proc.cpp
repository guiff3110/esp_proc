/*
  To use this code you will need the following dependancies:

  - Support for the ESP8266 boards.
        - You can add it to the board manager by going to File -> Preference and pasting
          http://arduino.esp8266.com/stable/package_esp8266com_index.json into the Additional Board Managers URL field.
        - Next, download the ESP8266 dependancies by going to Tools -> Board -> Board Manager and searching for ESP8266 and installing it.

  UPDATE 16 MAY 2017 by Knutella - Fixed MQTT disconnects when wifi drops by moving around Reconnect and adding a software reset of MCU

  UPDATE 23 MAY 2017 - The MQTT_MAX_PACKET_SIZE parameter may not be setting appropriately do to a bug in the PubSub library.
  If the MQTT messages are not being transmitted as expected please you may need to change
  the MQTT_MAX_PACKET_SIZE parameter in "PubSubClient.h" directly.

  UPDATE 02 DECEMBER 2017 by GURSO - Modify initialize, operating without wifi connection and try connect 10 times to wifi and mqtt, but fail realize software reset for MCU, add eeprom to save last output state

  SAMPLE PAYLOAD:
  {

  }
only buttons, no OutPutS
*/

//enable or disable features
//#define USE_I2C          // to use I2C, disable by //
//#define USE_TMP75        // to use TMP75, disable by //
//#define USE_DEBUG        // to use serial USE_DEBUG, disable by //
//#define USE_PCF8574A     // to use PCF8574A IO expander, disable by //
//#define USE_PCA9685      // to use PCF8574A IO expander, disable by //
//#define USE_LM75A        // to use TMP75, disable by //
//#define USE_TELE         // to use telemetry, disable by //
//#define USE_RGB          // to use RGB LEDS, disable by //

//includes
#include "config.h"
#include <FS.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>        // MQTT, Ota, WifiManager
#include <PubSubClient.h>       // MQTT
//#include <WiFiManager.h>        //https://github.com/tzapu/WiFiManager
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include <ESP8266mDNS.h>        // MQTT, Webserver
#include <EEPROM.h>
#include <Ticker.h>
#include <Adafruit_NeoPixel.h>


/**************************** PAYLOAD **************************************************/

const char* on_cmd = "ON";
const char* off_cmd = "OFF";

/**************************** TELEMETRY DEFINITIONS ********************************************/

#ifdef USE_TELE
 unsigned long telemetry_time = 60000;
 long lastMsg = 0;
 unsigned long now = 0;
#endif //USE_TELE

/**************************** BUTTON CHECK DEFINITIONS ********************************************/

unsigned long Button_check_time = 10;
long Button_lastTry = 0;
unsigned long Bnow = 0;

/**************************** RECONNECT DEFINITIONS ********************************************/

volatile int R = 0; //Attempt count to software reset
unsigned long reconnect_time = 10000;
long lastTry = 0;
unsigned long noww = 0;

/******************************** GLOBALS for LED STRIP *******************************/

// Maintained state for reporting to HA
byte red = 255;
byte green = 255;
byte blue = 255;
byte white = 255;
byte brightness = 255;
// Real values to write to the LEDs (ex. including brightness and state)
byte realRed = 0;
byte realGreen = 0;
byte realBlue = 0;
byte realWhite =  0;
//State of leds
bool stateOn = false;
//node mcu led
const int NodeLed = 16;

/**************************** NEOPIXELS DEFINITIONS *******************************************/

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN     2

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT  2

// NeoPixel brightness, 0 (min) to 255 (max)
#define BRIGHTNESS 50

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRBW + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)

/**************************** SERIAL BUFFER DEFINITIONS *******************************************/

const uint8_t RxBufferSize = 6;
const uint8_t TxBufferSize = 6;

uint8_t RxBuffer[RxBufferSize];
uint8_t TxBuffer[TxBufferSize];

// Define the orders that can be sent and received
enum Order {
  HELLO = 0, // Hello order to initiate communication with the Arduino
  ALREADY_CONNECTED = 1, // Already connected to the Arduino
  ERROR = 2,
  RECEIVED = 3, // Aknowlegment message
  SET_OUTPUT = 4, // Command the OUTPUT
  GET_OUTPUT = 5, // Receive Status OUTPUT
  SET_LED = 6,
  GET_LED = 8,
};

const uint8_t header = 0x7E;
const uint8_t endMsg = 0x8E;
uint8_t readCounter;
uint8_t isHeader;

//Flag that helps us restart counter when we first find header byte
uint8_t firstTimeHeader;

uint8_t verifyChecksum(uint8_t originalResult);


/**************************** MQTT BUFFER DEFINITIONS *******************************************/

const int BUFFER_SIZE = 300;

#define MQTT_MAX_PACKET_SIZE 512

/******************************** GLOBALS DEFINITIONS *******************************/

WiFiClient espClient;
PubSubClient client(espClient);
Ticker ticker;
bool shouldSaveConfig  = false;              //Flag para salvar os dados

/******************************** FUNCTIONS DEFINITIONS *******************************/

void software_Reset(void); // softreset
void reconnect(void); //reconnect function
void setup_wifi(void);
void OTA(void);
void tick(void);
void callback(char* topic, byte* payload, unsigned int length);
void set_output(bool OutState, int i);
void publishData(float p_temperature);
void telemetry(void);
bool processJson(char* message);
void sendState(int topic);
void sendStateW(int topic);
void ReceivedMsgCheck(void);
void Serial_Send_OutPutState(int output, int state);
void Serial_Send_LED(int led, int value);
uint8_t Serial_Receive_OutPutState(int i);
void UpdateOutPutState(void);
void UpdateLedState(void);
void Serial_Get_Telemetry(void);
uint8_t checksum(void);
uint8_t verifyChecksum(uint8_t originalResult);
void colorWipe(uint32_t color, int wait);
void rainbowFade2White(int wait, int rainbowLoops, int whiteLoops);
void pulseWhite(uint8_t wait);
void whiteOverRainbow(int whiteSpeed, int whiteLength);

/********************************** START SETUP*****************************************/

void setup() {

  //set output pins
  pinMode(NodeLed, OUTPUT);

  // start ticker with 0.2 because we start configuration
  ticker.attach(0.2, tick);

    // NeoPixels init
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)

    strip.setPixelColor(0, strip.Color(255, 0, 0, 0));         //  Set pixel's color (in RAM)
    strip.setPixelColor(1, strip.Color(255, 0, 0, 0));         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match


  //Serial
  Serial.begin(115200);
  delay(10);

  //we define our header byte only once, we're not going to change it
  TxBuffer[0] = header;
  TxBuffer[5] = endMsg;
  RxBuffer[0] = header;
  RxBuffer[5] = endMsg;

  //OTA
  OTA();

  //WIFI setup
  setup_wifi();

  //MQTT setup
  client.setServer(mqtt_server, atoi(mqtt_port));
  client.setCallback(callback);

  Serial.println("Ready");
  reconnect();
  #ifndef USE_DEBUG
    Serial.println("Serial off");
    delay(100);
    //Serial.end();
  #endif
}

/********************************** START MAIN LOOP***************************************/

void loop() {
  //OTA.Handler
  ArduinoOTA.handle();
  //Check connection and try to connect
  if (!client.connected()) {
    reconnect();
    //software_Reset(); if you prefer use this
  }
  //MQTT client loop
  client.loop();
  //telemetry check
  #ifdef USE_TELE
    telemetry();
  #endif

  ReceivedMsgCheck();

  // Fill along the length of the strip in various colors...
  //colorWipe(strip.Color(255,   0,   0)     , 100); // Red
  //colorWipe(strip.Color(  0, 255,   0)     , 50); // Green
  //colorWipe(strip.Color(  0,   0, 255)     , 100); // Blue
  //colorWipe(strip.Color(  0,   0,   0, 255), 100); // True white (not RGB white)
  //  strip.setPixelColor(0, strip.Color(255,   0,   0));         //  Set pixel's color (in RAM)
  //  strip.show();                          //  Update strip to match

  //whiteOverRainbow(75, 5);

  //pulseWhite(5);

  //rainbowFade2White(10, 3, 1);

  yield();  // take a breather, required for ESP8266
}

/********************************** START SETUP WIFI*****************************************/

void setup_wifi(void) {

  delay(10);
  Serial.println();
  Serial.println("Configuring WIFI");
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Try connecting to ");
  Serial.println(wifi_ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid, wifi_password);

  if(WiFi.status() != WL_CONNECTED){
    Serial.println("Wifi not conected");
  } else{
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

/********************************** Button *****************************************/
void set_output(bool OutState, int i)
{
  if(OutState)
  {
    #ifdef USE_DEBUG
    Serial.print("OUTPUT ");
    Serial.print(i);
    Serial.println(" ON");
    #endif
    Serial_Send_OutPutState(i, 1);
    //Serial_Send_OutPutState(i, OutPutState[i]);
    //Serial_Receive_OutPutState(i);
    //OutPutState[i] = true;
    //client.publish(state_topic[i], on_cmd, true);
  }
  else
  {
    #ifdef USE_DEBUG
    Serial.print("OUTPUT ");
    Serial.print(i);
    Serial.println(" OFF");
    #endif
    Serial_Send_OutPutState(i, 0);
    //OutPutState[i] = false;
    //client.publish(state_topic[i], off_cmd, true);
  }
  // Serial_Receive_OutPutState(i);


}

/********************************** Serial prep *****************************************/

void ReceivedMsgCheck(void){
  //Check if there is any data available to read
if(Serial.available() > 0){
  //read only one byte at a time
  uint8_t c = Serial.read();

  //Check if header is found
  if(c == header){
    if(!firstTimeHeader){
      isHeader = 1;
      readCounter = 0;
      firstTimeHeader = 1;
    }
  }

  //store received byte, increase readCounter
  RxBuffer[readCounter] = c;
  readCounter++;

  //prior overflow, we have to restart readCounter
  if(readCounter >= RxBufferSize){
    readCounter = 0;

    //if header was found
    if(isHeader){
      //get checksum value from buffer's last value, according to defined protocol
      uint8_t checksumValue = RxBuffer[4];

      //perform checksum validation, it's optional but really suggested
      if(verifyChecksum(checksumValue)){
        if(RxBuffer[1] == GET_OUTPUT){
          UpdateOutPutState();
        }
        if(RxBuffer[1] == GET_LED){
          //UpdateLedState();
        }
      }
      //restart header flag
      isHeader = 0;
      firstTimeHeader = 0;
    }
  }
}
}

/********************************** Serial prep *****************************************/

void UpdateLedState(void){

}

/********************************** Serial prep *****************************************/

void UpdateOutPutState(void){
  if(RxBuffer[3] == 01){
    uint8_t i = RxBuffer[2];
    OutPutState[i] = true;
    client.publish(state_topic[i], on_cmd, true);
  }
  else{
    uint8_t i = RxBuffer[2];
    OutPutState[i] = false;
    client.publish(state_topic[i], off_cmd, true);
  }
}

/********************************** Serial prep *****************************************/
uint8_t Serial_Receive_OutPutState(int i){

  TxBuffer[1] = GET_OUTPUT;
  TxBuffer[2] = i;
  TxBuffer[3] = 0;
  TxBuffer[4] = checksum();
  Serial.flush();
  Serial.write(TxBuffer, TxBufferSize);
}

/********************************** Serial send output *****************************************/
void Serial_Send_OutPutState(int output, int state){

  TxBuffer[1] = SET_OUTPUT;
  TxBuffer[2] = output;// && 0xFF;
  TxBuffer[3] = state;// && 0xFF;
  TxBuffer[4] = checksum();
  Serial.flush();
  Serial.write(TxBuffer, TxBufferSize);
}

/********************************** Serial send led *****************************************/
void Serial_Send_LED(int led, int value){

  TxBuffer[1] = SET_LED;
  TxBuffer[2] = led;// && 0xFF;
  TxBuffer[3] = value;// && 0xFF;
  TxBuffer[4] = checksum();
  Serial.flush();
  Serial.write(TxBuffer, TxBufferSize);
}

/********************************** START CALLBACK*****************************************/

void callback(char* topic, byte* payload, unsigned int length) {
  #ifdef USE_DEBUG
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  #endif //USE_DEBUG

  //topic to string
  String strTopic = String((char*)topic);
  strTopic.toLowerCase();

  //if set_topic[0] - output0
  if(strTopic == set_topic[0]){
    int x=0;
    //payload to string
    payload[length] = '\0';
    String strPayload = String((char*)payload);
    strPayload.toUpperCase();
    #ifdef USE_DEBUG
      Serial.println(strPayload);
    #endif
    if(strPayload == on_cmd){
      set_output(true, x);
      strip.setPixelColor(0, strip.Color(0,   0,   255));         //  Set pixel's color (in RAM)
      strip.show();                          //  Update strip to match
    }
    if(strPayload == off_cmd){
      set_output(false, x);
      strip.setPixelColor(0, strip.Color(255,   0,   0));         //  Set pixel's color (in RAM)
      strip.show();                          //  Update strip to match
    }
  }

  //if set_topic[1] - output0
  if(strTopic == set_topic[1]){
    int x=1;
    //payload to string
    payload[length] = '\0';
    String strPayload = String((char*)payload);
    strPayload.toUpperCase();
    #ifdef USE_DEBUG
      Serial.println(strPayload);
    #endif
    if(strPayload == on_cmd){
      set_output(true, x);
      strip.setPixelColor(1, strip.Color(0,   0,   255));         //  Set pixel's color (in RAM)
      strip.show();                          //  Update strip to match
    }
    if(strPayload == off_cmd){
      set_output(false, x);
      strip.setPixelColor(1, strip.Color(255,   0,   0));         //  Set pixel's color (in RAM)
      strip.show();                          //  Update strip to match
    }
  }

  //if set_topic[2]
  if(strTopic == set_topic[2]){
    int x=2;
    //payload to string
    payload[length] = '\0';
    String strPayload = String((char*)payload);
    strPayload.toUpperCase();
    #ifdef USE_DEBUG
      Serial.println(strPayload);
    #endif
    if(strPayload == on_cmd){
      set_output(true, x);
    }
    if(strPayload == off_cmd){
      set_output(false, x);
    }
  }

  //set_topic[3]
  if(strTopic == set_topic[3]){
    int topic = 3;
    char message[length + 1];
    for (unsigned int i = 0; i < length; i++) {
      message[i] = (char)payload[i];
    }
    message[length] = '\0';
    #ifdef USE_DEBUG
    Serial.println(message);
    #endif
    if (!processJson(message)) {
      return;
    }
    if (stateOn) {
      // Update lights
      realRed = map(red, 0, 255, 0, brightness);
      realGreen = map(green, 0, 255, 0, brightness);
      realBlue = map(blue, 0, 255, 0, brightness);

    }
    else {
      realRed = 0;
      realGreen = 0;
      realBlue = 0;

    }
    Serial_Send_LED(1, realRed);
    Serial_Send_LED(2, realGreen);
    Serial_Send_LED(3, realBlue);
    sendState(topic);
  }

  //set_topic[4]
  if(strTopic == set_topic[4]){
  int topic = 4;
  char message[length + 1];
  for (unsigned int i = 0; i < length; i++) {
    message[i] = (char)payload[i];
   }
  message[length] = '\0';
  #ifdef USE_DEBUG
  Serial.println(message);
  #endif

  if (!processJson(message)) {
    return;
   }
  if (stateOn) {
    // Update lights
    realWhite = brightness;
   }
  else {
    realWhite = 0;
   }
  Serial_Send_LED(0, realWhite);
  sendStateW(topic);
  }

}
/********************************** Json temperature*****************************************/

void publishData(float p_temperature) {
  // create a JSON object
  // doc : https://github.com/bblanchon/ArduinoJson/wiki/API%20Reference
  StaticJsonDocument<200> doc;
  // JsonObject root = doc.to<JsonObject>();
  // INFO: the data must be converted into a string; a problem occurs when using floats...
  doc["temperature"] = (String)p_temperature;
  #ifdef USE_DEBUG
  serializeJson(doc, Serial);
  Serial.println("");
  /*
     {
        "temperature": "23.20" ,
        "humidity": "43.70"
     }
  */
  #endif //USE_DEBUG
  char data[200];
  serializeJson(doc, data, measureJson(doc) + 1);
  client.publish(telemetry_topic, data, true);
}

/********************************** Telemetry ***************************************/

#ifdef USE_TELE
void telemetry(void){
  now = millis();
  if (now - lastMsg > telemetry_time){
    lastMsg = now;
    // publishData(lm75a_sensor.getTemperatureInDegrees());
  }
}
#endif //USE_TELE

/********************************** OTA *****************************************/

void OTA(void)
{
  //OTA
  ArduinoOTA.setPort(OTAport);

  ArduinoOTA.setHostname(NODENAME);

  ArduinoOTA.setPassword((const char *)OTApassword);

  ArduinoOTA.onStart([]() {
    Serial.println("Starting");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
}

/********************************** START PROCESS JSON*****************************************/

bool processJson(char* message) {

  StaticJsonDocument<BUFFER_SIZE> doc;
  JsonObject root = doc.to<JsonObject>();
  auto error = deserializeJson(doc, message);

  if (error)
  {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(error.c_str());
    return false;
  }

  if (root.containsKey("state")) {
    if (strcmp(root["state"], on_cmd) == 0) {
      stateOn = true;
    }
    else if (strcmp(root["state"], off_cmd) == 0) {
      stateOn = false;
    }
  }
  if (root.containsKey("color")) {
    red = root["color"]["r"];
    green = root["color"]["g"];
    blue = root["color"]["b"];
  }

  if (root.containsKey("brightness")) {
    brightness = root["brightness"];
  }

  return true;
}

/********************************** START SEND STATE*****************************************/

void sendState(int topic) {

  StaticJsonDocument<BUFFER_SIZE> doc;

  JsonObject root = doc.to<JsonObject>();

  root["state"] = (stateOn) ? on_cmd : off_cmd;
  JsonObject color = root.createNestedObject("color");
  color["r"] = red;
  color["g"] = green;
  color["b"] = blue;

  root["brightness"] = brightness;

  char buffer[measureJson(doc) + 1];
  serializeJson(doc, buffer, sizeof(buffer));
  #ifdef USE_DEBUG
  Serial.println(buffer);
  #endif
  client.publish(state_topic[topic], buffer, true);
}

/********************************** START SEND STATE*****************************************/

void sendStateW(int topic) {

  StaticJsonDocument<BUFFER_SIZE> doc;

  doc["state"] = (stateOn) ? on_cmd : off_cmd;

  doc["brightness"] = brightness;

  char buffer[measureJson(doc) + 1];
  serializeJson(doc, buffer, sizeof(buffer));
  #ifdef USE_DEBUG
  Serial.println(buffer);
  #endif
  client.publish(state_topic[topic], buffer, true);
}

/********************************** RECONNECT *****************************************/

void reconnect(void) {
  noww = millis();
  if (noww - lastTry > reconnect_time){
    lastTry = noww;
    // start ticker with 0.5 because we start in AP mode and try to connect
    ticker.attach(0.5, tick);
    #ifdef USE_DEBUG
    Serial.print("Attempting MQTT connection...");
    #endif
    // Attempt to connect
    if (client.connect(NODENAME, mqtt_user, mqtt_password)) {
      #ifdef USE_DEBUG
      Serial.println("connected");
      Serial.println("Node name:" NODENAME);
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      #endif
      //topics to subscribe
      client.subscribe(set_topic[0]); // button 0 topic
      client.subscribe(set_topic[1]); // button 1 topic
      client.subscribe(set_topic[2]); // button 2 topic
      client.subscribe(set_topic[3]); // rgb led topic
      client.subscribe(set_topic[4]); // white led topic
      R = 0;
      ticker.detach();
      digitalWrite(NodeLed,HIGH);
    }
    else {
    #ifdef USE_DEBUG
     Serial.print("failed, rc=");
     Serial.print(client.state());
     Serial.println(" try again ");
     #endif
     R++;
     #ifdef USE_DEBUG
     Serial.print("Retry count: ");
     Serial.println(R);
     #endif
     if (R >= 10){
       software_Reset();
     }
    }
  }
}

/********************************** CheckSun *****************************************/

//We perform a sum of all bytes, except the one that corresponds to the original
//checksum value. After summing we need to AND the result to a byte value.
uint8_t checksum(void){
  uint8_t result = 0;
  uint16_t sum = 0;

  for(uint8_t i = 0; i < (TxBufferSize - 2); i++){
    sum += TxBuffer[i];
  }
  result = sum & 0xFF;

  return result;
}
/********************************** Valid CheckSun *****************************************/
//This a common checksum validation method
//We perform a sum of all bytes, except the one that corresponds to the original
//checksum value. After summing we need to AND the result to a byte value.
uint8_t verifyChecksum(uint8_t originalResult){
  uint8_t result = 0;
  uint16_t sum = 0;

  for(uint8_t i = 0; i < (RxBufferSize - 2); i++){
    sum += RxBuffer[i];
  }
  result = sum & 0xFF;

  if(originalResult == result){
     return 1;
  }else{
     return 0;
  }
}

// Fill strip pixels one after another with a color. Strip is NOT cleared
// first; anything there will be covered pixel by pixel. Pass in color
// (as a single 'packed' 32-bit value, which you can get by calling
// strip.Color(red, green, blue) as shown in the loop() function above),
// and a delay time (in milliseconds) between pixels.
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

void whiteOverRainbow(int whiteSpeed, int whiteLength) {

  if(whiteLength >= strip.numPixels()) whiteLength = strip.numPixels() - 1;

  int      head          = whiteLength - 1;
  int      tail          = 0;
  int      loops         = 3;
  int      loopNum       = 0;
  uint32_t lastTime      = millis();
  uint32_t firstPixelHue = 0;

  for(;;) { // Repeat forever (or until a 'break' or 'return')
    for(int i=0; i<strip.numPixels(); i++) {  // For each pixel in strip...
      if(((i >= tail) && (i <= head)) ||      //  If between head & tail...
         ((tail > head) && ((i >= tail) || (i <= head)))) {
        strip.setPixelColor(i, strip.Color(0, 0, 0, 255)); // Set white
      } else {                                             // else set rainbow
        int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
        strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
      }
    }

    strip.show(); // Update strip with new contents
    // There's no delay here, it just runs full-tilt until the timer and
    // counter combination below runs out.

    firstPixelHue += 40; // Advance just a little along the color wheel

    if((millis() - lastTime) > whiteSpeed) { // Time to update head/tail?
      if(++head >= strip.numPixels()) {      // Advance head, wrap around
        head = 0;
        if(++loopNum >= loops) return;
      }
      if(++tail >= strip.numPixels()) {      // Advance tail, wrap around
        tail = 0;
      }
      lastTime = millis();                   // Save time of last movement
    }
  }
}

void pulseWhite(uint8_t wait) {
  for(int j=0; j<256; j++) { // Ramp up from 0 to 255
    // Fill entire strip with white at gamma-corrected brightness level 'j':
    strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
    strip.show();
    delay(wait);
  }

  for(int j=255; j>=0; j--) { // Ramp down from 255 to 0
    strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
    strip.show();
    delay(wait);
  }
}

void rainbowFade2White(int wait, int rainbowLoops, int whiteLoops) {
  int fadeVal=0, fadeMax=100;

  // Hue of first pixel runs 'rainbowLoops' complete loops through the color
  // wheel. Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to rainbowLoops*65536, using steps of 256 so we
  // advance around the wheel at a decent clip.
  for(uint32_t firstPixelHue = 0; firstPixelHue < rainbowLoops*65536;
    firstPixelHue += 256) {

    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the three-argument variant, though the
      // second value (saturation) is a constant 255.
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }

    strip.show();
    delay(wait);

    if(firstPixelHue < 65536) {                              // First loop,
      if(fadeVal < fadeMax) fadeVal++;                       // fade in
    } else if(firstPixelHue >= ((rainbowLoops-1) * 65536)) { // Last loop,
      if(fadeVal > 0) fadeVal--;                             // fade out
    } else {
      fadeVal = fadeMax; // Interim loop, make sure fade is at max
    }
  }

  for(int k=0; k<whiteLoops; k++) {
    for(int j=0; j<256; j++) { // Ramp up 0 to 255
      // Fill entire strip with white at gamma-corrected brightness level 'j':
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
    delay(1000); // Pause 1 second
    for(int j=255; j>=0; j--) { // Ramp down 255 to 0
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
  }

  delay(500); // Pause 1/2 second
}

/********************************** TICK LED *****************************************/
void tick(void)
{
  int state = digitalRead(NodeLed);  // get the current state of GPIO16 pin
  digitalWrite(NodeLed, !state); // set pin to the opposite state
}
/********************************** Reset *****************************************/
void software_Reset() // Restarts program from beginning but does not reset the peripherals and registers
{
Serial.print("resetting");
ESP.reset();
}
