//configs

#ifndef config_h
#define config_h

/************ WIFI INFORMATION (CHANGE THESE FOR YOUR SETUP) ******************/

#define wifi_ssid "ManiacIOT" //wifi ssid
#define wifi_password "hipopotamo" // wifi password

/************ MQTT INFORMATION (CHANGE THESE FOR YOUR SETUP) ******************/

#define mqtt_server "192.168.1.53"  //ip from mqtt broker
#define mqtt_user "guiBroker"       //user name used on mqtt broker
#define mqtt_password "huahua209"   //password used on mqtt broker
#define mqtt_port  "1883"           //port used on mqtt broker

/************* MQTT TOPICS (change these topics as you wish)  **************************/

#ifdef NODE_3
#define NODENAME "node3"
#define topics 4
const char* state_topic[topics] = {"stat/node3/ledw", "stat/node3/out0", "stat/node6/out0", "stat/node3/led"};
const char* set_topic[topics] = {"cmnd/node3/ledw", "cmnd/node3/out0", "cmnd/node6/out0", "cmnd/node3/led"};
const char* telemetry_topic ={"tele/node3"};
#endif  // NODE_3

#ifdef NODE_4
#define NODENAME "node4"
#define topics 4
const char* state_topic[topics] = {"stat/node4/out0", "stat/node10/out0", "stat/node9/out1", "stat/node4/led"};
const char* set_topic[topics] = {"cmnd/node4/out0", "cmnd/node10/out0", "cmnd/node9/out1", "cmnd/node4/led"};
const char* telemetry_topic ={"tele/node4"};
#endif  // NODE_4

#ifdef NODE_5
#define NODENAME "node5"
#define topics 2
const char* state_topic[topics] = {"stat/node5/out0", "stat/node5/out1"};
const char* set_topic[topics] = {"cmnd/node5/out0", "cmnd/node5/out1"};
const char* telemetry_topic ={"tele/node5"};
#endif  // NODE_5

#ifdef NODE_6
#define NODENAME "node6"
#define topics 2
const char* state_topic[topics] = {"stat/node6/out0", "stat/node6/out1"};
const char* set_topic[topics] = {"cmnd/node6/out0", "cmnd/node6/out1"};
const char* telemetry_topic = {"tele/node6"};
#endif  // NODE_6

#ifdef NODE_7
#define NODENAME "node7"
#define topics 2
const char* state_topic[topics] = {"stat/node7/out0", "stat/node7/out1"};
const char* set_topic[topics] = {"cmnd/node7/out0", "cmnd/node7/out1"};
const char* telemetry_topic ={"tele/node7"};
#endif  // NODE_7

#ifdef NODE_9
#define NODENAME "node9"
#define topics 2
const char* state_topic[topics] = {"stat/node9/out0", "stat/node9/out1"};
const char* set_topic[topics] = {"cmnd/node9/out0", "cmnd/node9/out1"};
const char* telemetry_topic = {"tele/node9"};
#endif  // NODE_9

#ifdef NODE_10
#define NODENAME "node10"
#define topics 2
const char* state_topic[topics] = {"stat/node10/out0", "stat/node10/out1"};
const char* set_topic[topics] = {"cmnd/node10/out0", "cmnd/node10/out1"};
const char* telemetry_topic =  {"tele/node10"};
#endif  // NODE_10

#ifdef NODE_11
#define NODENAME "node11"
#define topics 4
const char* state_topic[topics] = {"stat/node5/out0", "stat/node5/out1", "stat/node7/out0", "stat/node11/led"};
const char* set_topic[topics] = {"cmnd/node5/out0", "cmnd/node5/out1", "cmnd/node7/out0", "cmnd/node11/led"};
const char* telemetry_topic =  {"tele/node11"};
#endif  // NODE_11

#ifdef NODE_12
#define NODENAME "node12"
#define topics 4
const char* state_topic[topics] = {"stat/node12/out0", "stat/node12/out1", "stat/node12/out2", "stat/node12/led"};
const char* set_topic[topics] = {"cmnd/node12/out0", "cmnd/node12/out1", "cmnd/node12/out2", "cmnd/node12/led"};
const char* telemetry_topic = {"tele/node12"};
#endif //NODE_12

#ifdef NODE_13
#define NODENAME "node13"
#define topics 4
const char* state_topic[topics] = {"stat/node9/out0", "stat/node9/out1", "stat/node7/out0", "stat/node13/led"};
const char* set_topic[topics] = {"cmnd/node9/out0", "cmnd/node9/out1", "cmnd/node7/out0", "cmnd/node13/led"};
const char* telemetry_topic =  {"tele/node13"};
#endif  // NODE_13

#ifdef NODE_14
#define NODENAME "node14"
#define topics 5
const char* state_topic[topics] = {"stat/node14/out0", "stat/node14/out1", "stat/node14/out0", "stat/node14/led", "stat/node14/ledw"};
const char* set_topic[topics] = {"cmnd/node14/out0", "cmnd/node14/out1", "cmnd/node14/out0", "cmnd/node14/led", "cmnd/node14/ledw"};
const char* telemetry_topic =  {"tele/node14"};
#endif  // NODE_14

#ifdef NODE_15
#define NODENAME "node15"
#define topics 5
const char* state_topic[topics] = {"stat/node15/out0", "stat/node15/out1", "stat/node15/out2", "stat/node15/led", "stat/node15/ledw"};
const char* set_topic[topics] = {"cmnd/node15/out0", "cmnd/node15/out1", "cmnd/node15/out2", "cmnd/node15/led", "cmnd/node15/ledw"};
const char* telemetry_topic =  {"tele/node15"};
#endif  // NODE_15

#ifdef NODE_16
#define NODENAME "node16"
#define topics 4
const char* state_topic[topics] = {"stat/node9/out0", "stat/node9/out1", "stat/node7/out0", "stat/node13/led"};
const char* set_topic[topics] = {"cmnd/node9/out0", "cmnd/node9/out1", "cmnd/node7/out0", "cmnd/node13/led"};
const char* telemetry_topic =  {"tele/node13"};
#endif  // NODE_16

#ifdef NODE_17
#define NODENAME "node17"
#define topics 4
const char* state_topic[topics] = {"stat/node9/out0", "stat/node9/out1", "stat/node7/out0", "stat/node13/led"};
const char* set_topic[topics] = {"cmnd/node9/out0", "cmnd/node9/out1", "cmnd/node7/out0", "cmnd/node13/led"};
const char* telemetry_topic =  {"tele/node13"};
#endif  // NODE_17

/**************************** FOR OTA **************************************************/

#define OTApassword "huahua209" // change this to whatever password you want to use when you upload OTA
int OTAport = 8266;

/**************************** BUTTON DEFINITIONS ********************************************/

#ifdef NODE_3
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_3

#ifdef NODE_4
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_4

#ifdef NODE_5
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_5

#ifdef NODE_6
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_6

#ifdef NODE_7
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_7

#ifdef NODE_8
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_8

#ifdef NODE_9
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_9

#ifdef NODE_10
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_10

#ifdef NODE_11
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_11

#ifdef NODE_12
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_12

#ifdef NODE_13
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_13

#ifdef NODE_14
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_14

#ifdef NODE_15
// #define MAX_SWITCHS 3
// const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
// unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
// unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
// volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
// volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_15

#ifdef NODE_16
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_16

#ifdef NODE_17
#define MAX_SWITCHS 3
const int SW[MAX_SWITCHS] = {4, 5, 16}; // set the switch pins {GPIO00, GPIO05, GPIO04}
unsigned long lastDebounceTime[MAX_SWITCHS] = {0, 0, 0};  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
volatile int lastSwState[MAX_SWITCHS]= {0, 0, 0};
volatile int buttonState[MAX_SWITCHS]= {0, 0, 0};
#endif //NODE_17

/******************************** OUTPUT PINS DEFINITIONS *******************************/

#ifdef NODE_3
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins {GPIO16, GPIO13}
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_3

#ifdef NODE_4
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_4

#ifdef NODE_5
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_5

#ifdef NODE_6
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_6

#ifdef NODE_7
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_7

#ifdef NODE_8
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_8

#ifdef NODE_9
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_9

#ifdef NODE_10
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_10

#ifdef NODE_11
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_11

#ifdef NODE_12
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_12

#ifdef NODE_13
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_13

#ifdef NODE_14
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_14

#ifdef NODE_15
#define MAX_OUTPUT_PINS 3
//const int OutputPins[MAX_OUTPUT_PINS] = {16}; // set the output pins
volatile int OutPutState[MAX_OUTPUT_PINS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
// volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_15

#ifdef NODE_16
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_16

#ifdef NODE_17
#define MAX_OUTPUT_PINS 3
const int OutputPins[MAX_OUTPUT_PINS] = {14, 12, 13}; // set the output pins
volatile int OutPutState[MAX_SWITCHS]= {0, 0, 0};
const char* OUT[MAX_OUTPUT_PINS] = {"OUT0", "OUT1", "OUT2"};
volatile int allocated_mem[MAX_OUTPUT_PINS] = {4, 4, 4};
#endif //NODE_17


#endif
